﻿using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Json
{
    public partial class FindForm : Form
    {
        private TextEditorControl TE;

        private int left_x, top_y;

        private List<TextMarker> markers = new List<TextMarker>();

        public FindForm(TextEditorControl TextEdit, int x, int y)
        {
            InitializeComponent();
            TE = TextEdit;
            left_x = x;
            top_y = y;
        }

        private void FindForm_Load(object sender, EventArgs e)
        {
            Left = left_x - (Width / 2);
            Top = top_y - (Height / 2);

            查找内容编辑框.Text = TE.ActiveTextAreaControl.SelectionManager.SelectedText;
        }

        private void FindForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            清除选中高亮();
        }

        private void 查找按钮_Click(object sender, EventArgs e)
        {
            查找文本();
        }

        private void 替换按钮_Click(object sender, EventArgs e)
        {
            int startindex;
            TextLocation end, start;

            if (查找内容编辑框.Text.Length < 1) return;

            if(TE.ActiveTextAreaControl.SelectionManager.SelectedText != 查找内容编辑框.Text || TE.ActiveTextAreaControl.SelectionManager.SelectedText.Length < 1)
            {
                if (!查找文本()) return;
                startindex = TE.ActiveTextAreaControl.TextArea.Caret.Offset + 2 - TE.ActiveTextAreaControl.SelectionManager.SelectedText.Length;
            }
            else
            {
                startindex = TE.ActiveTextAreaControl.TextArea.Caret.Offset - TE.ActiveTextAreaControl.SelectionManager.SelectedText.Length;
            }

            TE.Document.UndoStack.StartUndoGroup();

            TE.ActiveTextAreaControl.TextArea.Caret.Position = TE.Document.OffsetToPosition(startindex);
            TE.ActiveTextAreaControl.TextArea.Document.Remove(startindex, TE.ActiveTextAreaControl.SelectionManager.SelectedText.Length);
            TE.ActiveTextAreaControl.TextArea.Document.Insert(startindex, 替换编辑框.Text);

            start = TE.Document.OffsetToPosition(startindex);
            end = TE.Document.OffsetToPosition(startindex + 替换编辑框.Text.Length);

            TE.ActiveTextAreaControl.TextArea.Caret.Position = TE.Document.OffsetToPosition(startindex + 替换编辑框.Text.Length);
            TE.ActiveTextAreaControl.SelectionManager.SetSelection(new DefaultSelection(TE.Document, start, end));

            TE.Document.UndoStack.EndUndoGroup();
        }

        private void 全部替换按钮_Click(object sender, EventArgs e)
        {
            if (查找内容编辑框.Text.Length < 1) return;

            TE.ActiveTextAreaControl.Caret.Position = TE.Document.OffsetToPosition(0);
            string text = 查找内容编辑框.Text;
            StringComparison type;

            if (区分大小写选择框.Checked)
            {
                type = StringComparison.CurrentCulture;
            }
            else
            {
                type = StringComparison.CurrentCultureIgnoreCase;
            }

            if (TE.Text.IndexOf(text, 0, type) < 0)
            {
                MessageBox.Show("找不到\"" + 查找内容编辑框.Text + "\"", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            TE.Document.UndoStack.StartUndoGroup();

            while (true)
            {
                int startindex = TE.ActiveTextAreaControl.TextArea.Caret.Offset;
                
                int offset = TE.Text.IndexOf(text, startindex, type);

                if (offset < 0) break;

                TE.ActiveTextAreaControl.TextArea.Document.Remove(offset, text.Length);
                TE.ActiveTextAreaControl.TextArea.Document.Insert(offset, 替换编辑框.Text);
            }

            TE.Document.UndoStack.EndUndoGroup();
        }

        private void FindForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) Close();
        }

        private void 查找内容编辑框_TextChanged(object sender, EventArgs e)
        {
            清除选中高亮();

            if (查找内容编辑框.Text.Length < 1) return;

            TE.ActiveTextAreaControl.Caret.Position = TE.Document.OffsetToPosition(0);
            string text = 查找内容编辑框.Text;
            StringComparison type;

            if (区分大小写选择框.Checked)
            {
                type = StringComparison.CurrentCulture;
            }
            else
            {
                type = StringComparison.CurrentCultureIgnoreCase;
            }

            if (TE.Text.IndexOf(text, 0, type) < 0)
            {
                return;
            }

            int startindex = 0;

            while (true)
            {
                int offset = TE.Text.IndexOf(text, startindex, type);

                if (offset < 0) break;

                TextMarker tm = new TextMarker(offset, text.Length, TextMarkerType.SolidBlock, Color.Yellow, Color.Black);

                markers.Add(tm);
                TE.Document.MarkerStrategy.AddMarker(tm);

                startindex = offset + text.Length;
            }

            TE.Refresh();
        }

        public void 清除选中高亮()
        {
            foreach (TextMarker m in markers) TE.Document.MarkerStrategy.RemoveMarker(m);

            markers.Clear();
            TE.Refresh();
        }

        public bool 查找文本(bool msg = true)
        {
            if (向下单选框.Checked)
            {
                int startindex = TE.ActiveTextAreaControl.TextArea.Caret.Offset;
                string text = 查找内容编辑框.Text;
                StringComparison type;
                if (区分大小写选择框.Checked)
                {
                    type = StringComparison.CurrentCulture;
                }
                else
                {
                    type = StringComparison.CurrentCultureIgnoreCase;
                }

                if (TE.Text.IndexOf(text, 0, type) < 0)
                {
                    MessageBox.Show("找不到\"" + 查找内容编辑框.Text + "\"", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                int offset = TE.Text.IndexOf(text, startindex, type);

                if (offset >= 0)
                {
                    var start = TE.Document.OffsetToPosition(offset);
                    var end = TE.Document.OffsetToPosition(offset + text.Length);
                    TE.ActiveTextAreaControl.SelectionManager.SetSelection(new DefaultSelection(TE.Document, start, end));

                    TE.ActiveTextAreaControl.Caret.Position = TE.Document.OffsetToPosition(offset + text.Length - 2);
                    TE.ActiveTextAreaControl.TextArea.ScrollToCaret();
                }
                else
                {
                    if (循环选择框.Checked)
                    {
                        offset = TE.Text.IndexOf(text, 0, type);
                        var start = TE.Document.OffsetToPosition(offset);
                        var end = TE.Document.OffsetToPosition(offset + text.Length);
                        TE.ActiveTextAreaControl.SelectionManager.SetSelection(new DefaultSelection(TE.Document, start, end));

                        TE.ActiveTextAreaControl.Caret.Position = TE.Document.OffsetToPosition(offset + text.Length - 2);
                        TE.ActiveTextAreaControl.TextArea.ScrollToCaret();
                    }
                    else
                    {
                        if (msg) MessageBox.Show("找不到\"" + 查找内容编辑框.Text + "\"", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            else
            {
                int startindex = TE.ActiveTextAreaControl.TextArea.Caret.Offset;
                string text = 查找内容编辑框.Text;
                StringComparison type;
                if (区分大小写选择框.Checked)
                {
                    type = StringComparison.CurrentCulture;
                }
                else
                {
                    type = StringComparison.CurrentCultureIgnoreCase;
                }

                if (TE.Text.IndexOf(text, 0, type) < 0)
                {
                    MessageBox.Show("找不到\"" + 查找内容编辑框.Text + "\"", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                int offset = TE.Text.LastIndexOf(text, startindex, type);

                if (offset >= 0)
                {
                    var start = TE.Document.OffsetToPosition(offset);
                    var end = TE.Document.OffsetToPosition(offset + text.Length);
                    TE.ActiveTextAreaControl.SelectionManager.SetSelection(new DefaultSelection(TE.Document, start, end));

                    TE.ActiveTextAreaControl.Caret.Position = TE.Document.OffsetToPosition(offset + text.Length - 2);
                    TE.ActiveTextAreaControl.TextArea.ScrollToCaret();
                }
                else
                {
                    if (循环选择框.Checked)
                    {
                        offset = TE.Text.LastIndexOf(text, TE.Text.Length, type);
                        var start = TE.Document.OffsetToPosition(offset);
                        var end = TE.Document.OffsetToPosition(offset + text.Length);
                        TE.ActiveTextAreaControl.SelectionManager.SetSelection(new DefaultSelection(TE.Document, start, end));

                        TE.ActiveTextAreaControl.Caret.Position = TE.Document.OffsetToPosition(offset + text.Length - 2);
                        TE.ActiveTextAreaControl.TextArea.ScrollToCaret();
                    }
                    else
                    {
                        if (msg) MessageBox.Show("找不到\"" + 查找内容编辑框.Text + "\"", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }

            return true;
        }

    }
}
