﻿
namespace Json
{
    partial class Json解析窗口
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Json解析窗口));
            this.Json代码编辑框 = new ICSharpCode.TextEditor.TextEditorControl();
            this.解析按钮 = new System.Windows.Forms.Button();
            this.清除按钮 = new System.Windows.Forms.Button();
            this.代码框右键菜单 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.查找替换右键 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.格式化右键 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.撤销右键 = new System.Windows.Forms.ToolStripMenuItem();
            this.重做右键 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.复制右键 = new System.Windows.Forms.ToolStripMenuItem();
            this.粘贴右键 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.全选右键 = new System.Windows.Forms.ToolStripMenuItem();
            this.顶部菜单 = new System.Windows.Forms.MenuStrip();
            this.文件菜单 = new System.Windows.Forms.ToolStripMenuItem();
            this.打开文件 = new System.Windows.Forms.ToolStripMenuItem();
            this.保存文件 = new System.Windows.Forms.ToolStripMenuItem();
            this.另存为文件 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.退出文件 = new System.Windows.Forms.ToolStripMenuItem();
            this.编辑菜单 = new System.Windows.Forms.ToolStripMenuItem();
            this.查找替换编辑 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.格式化编辑 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.撤销编辑 = new System.Windows.Forms.ToolStripMenuItem();
            this.重做编辑 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.复制编辑 = new System.Windows.Forms.ToolStripMenuItem();
            this.粘贴编辑 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.全选编辑 = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助菜单 = new System.Windows.Forms.ToolStripMenuItem();
            this.检查更新帮助 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.Gitee帮助 = new System.Windows.Forms.ToolStripMenuItem();
            this.关于帮助 = new System.Windows.Forms.ToolStripMenuItem();
            this.压缩按钮 = new System.Windows.Forms.Button();
            this.代码框右键菜单.SuspendLayout();
            this.顶部菜单.SuspendLayout();
            this.SuspendLayout();
            // 
            // Json代码编辑框
            // 
            this.Json代码编辑框.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Json代码编辑框.BackColor = System.Drawing.Color.Transparent;
            this.Json代码编辑框.IsReadOnly = false;
            this.Json代码编辑框.Location = new System.Drawing.Point(0, 28);
            this.Json代码编辑框.Margin = new System.Windows.Forms.Padding(2);
            this.Json代码编辑框.Name = "Json代码编辑框";
            this.Json代码编辑框.ShowSpaces = true;
            this.Json代码编辑框.ShowTabs = true;
            this.Json代码编辑框.ShowVRuler = false;
            this.Json代码编辑框.Size = new System.Drawing.Size(657, 783);
            this.Json代码编辑框.TabIndex = 0;
            this.Json代码编辑框.TextChanged += new System.EventHandler(this.Json代码编辑框_TextChanged);
            // 
            // 解析按钮
            // 
            this.解析按钮.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.解析按钮.Location = new System.Drawing.Point(661, 27);
            this.解析按钮.Name = "解析按钮";
            this.解析按钮.Size = new System.Drawing.Size(70, 38);
            this.解析按钮.TabIndex = 1;
            this.解析按钮.Text = "格式化";
            this.解析按钮.UseVisualStyleBackColor = true;
            this.解析按钮.Click += new System.EventHandler(this.解析按钮_Click);
            // 
            // 清除按钮
            // 
            this.清除按钮.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.清除按钮.Location = new System.Drawing.Point(661, 111);
            this.清除按钮.Name = "清除按钮";
            this.清除按钮.Size = new System.Drawing.Size(70, 38);
            this.清除按钮.TabIndex = 2;
            this.清除按钮.Text = "清空";
            this.清除按钮.UseVisualStyleBackColor = true;
            this.清除按钮.Click += new System.EventHandler(this.清除按钮_Click);
            // 
            // 代码框右键菜单
            // 
            this.代码框右键菜单.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查找替换右键,
            this.toolStripSeparator4,
            this.格式化右键,
            this.toolStripSeparator9,
            this.撤销右键,
            this.重做右键,
            this.toolStripSeparator7,
            this.复制右键,
            this.粘贴右键,
            this.toolStripSeparator5,
            this.全选右键});
            this.代码框右键菜单.Name = "代码框右键菜单";
            this.代码框右键菜单.Size = new System.Drawing.Size(140, 182);
            // 
            // 查找替换右键
            // 
            this.查找替换右键.Name = "查找替换右键";
            this.查找替换右键.ShowShortcutKeys = false;
            this.查找替换右键.Size = new System.Drawing.Size(139, 22);
            this.查找替换右键.Text = "查找/替换 (&F)";
            this.查找替换右键.Click += new System.EventHandler(this.查找替换编辑_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(136, 6);
            // 
            // 格式化右键
            // 
            this.格式化右键.Name = "格式化右键";
            this.格式化右键.Size = new System.Drawing.Size(139, 22);
            this.格式化右键.Text = "格式化 (&D)";
            this.格式化右键.Click += new System.EventHandler(this.解析按钮_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(136, 6);
            // 
            // 撤销右键
            // 
            this.撤销右键.Name = "撤销右键";
            this.撤销右键.Size = new System.Drawing.Size(139, 22);
            this.撤销右键.Text = "撤销 (&U)";
            this.撤销右键.Click += new System.EventHandler(this.撤销_Click);
            // 
            // 重做右键
            // 
            this.重做右键.Name = "重做右键";
            this.重做右键.Size = new System.Drawing.Size(139, 22);
            this.重做右键.Text = "重做 (&R)";
            this.重做右键.Click += new System.EventHandler(this.重做_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(136, 6);
            // 
            // 复制右键
            // 
            this.复制右键.Name = "复制右键";
            this.复制右键.ShowShortcutKeys = false;
            this.复制右键.Size = new System.Drawing.Size(139, 22);
            this.复制右键.Text = "复制 (&C)";
            this.复制右键.Click += new System.EventHandler(this.复制_Click);
            // 
            // 粘贴右键
            // 
            this.粘贴右键.Name = "粘贴右键";
            this.粘贴右键.ShowShortcutKeys = false;
            this.粘贴右键.Size = new System.Drawing.Size(139, 22);
            this.粘贴右键.Text = "粘贴 (&P)";
            this.粘贴右键.Click += new System.EventHandler(this.粘贴_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(136, 6);
            // 
            // 全选右键
            // 
            this.全选右键.Name = "全选右键";
            this.全选右键.ShowShortcutKeys = false;
            this.全选右键.Size = new System.Drawing.Size(139, 22);
            this.全选右键.Text = "全选 (&A)";
            this.全选右键.Click += new System.EventHandler(this.全选_Click);
            // 
            // 顶部菜单
            // 
            this.顶部菜单.BackColor = System.Drawing.SystemColors.Control;
            this.顶部菜单.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件菜单,
            this.编辑菜单,
            this.帮助菜单});
            this.顶部菜单.Location = new System.Drawing.Point(0, 0);
            this.顶部菜单.Name = "顶部菜单";
            this.顶部菜单.Size = new System.Drawing.Size(735, 25);
            this.顶部菜单.TabIndex = 3;
            this.顶部菜单.Text = "顶部菜单";
            // 
            // 文件菜单
            // 
            this.文件菜单.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开文件,
            this.保存文件,
            this.另存为文件,
            this.toolStripSeparator1,
            this.退出文件});
            this.文件菜单.Name = "文件菜单";
            this.文件菜单.Size = new System.Drawing.Size(62, 21);
            this.文件菜单.Text = "文件 (&F)";
            // 
            // 打开文件
            // 
            this.打开文件.Name = "打开文件";
            this.打开文件.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.打开文件.Size = new System.Drawing.Size(210, 22);
            this.打开文件.Text = "打开 (&O)";
            this.打开文件.Click += new System.EventHandler(this.打开文件_Click);
            // 
            // 保存文件
            // 
            this.保存文件.Name = "保存文件";
            this.保存文件.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.保存文件.Size = new System.Drawing.Size(210, 22);
            this.保存文件.Text = "保存 (&S)";
            this.保存文件.Click += new System.EventHandler(this.保存文件_Click);
            // 
            // 另存为文件
            // 
            this.另存为文件.Name = "另存为文件";
            this.另存为文件.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.另存为文件.Size = new System.Drawing.Size(210, 22);
            this.另存为文件.Text = "另存为 (&A)";
            this.另存为文件.Click += new System.EventHandler(this.另存为文件_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(207, 6);
            // 
            // 退出文件
            // 
            this.退出文件.Name = "退出文件";
            this.退出文件.Size = new System.Drawing.Size(210, 22);
            this.退出文件.Text = "退出 (&X)";
            this.退出文件.Click += new System.EventHandler(this.退出文件_Click);
            // 
            // 编辑菜单
            // 
            this.编辑菜单.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查找替换编辑,
            this.toolStripSeparator2,
            this.格式化编辑,
            this.toolStripSeparator10,
            this.撤销编辑,
            this.重做编辑,
            this.toolStripSeparator6,
            this.复制编辑,
            this.粘贴编辑,
            this.toolStripSeparator3,
            this.全选编辑});
            this.编辑菜单.Name = "编辑菜单";
            this.编辑菜单.Size = new System.Drawing.Size(63, 21);
            this.编辑菜单.Text = "编辑 (&E)";
            this.编辑菜单.Click += new System.EventHandler(this.编辑菜单_Click);
            // 
            // 查找替换编辑
            // 
            this.查找替换编辑.Name = "查找替换编辑";
            this.查找替换编辑.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.查找替换编辑.Size = new System.Drawing.Size(190, 22);
            this.查找替换编辑.Text = "查找/替换 (&F)";
            this.查找替换编辑.Click += new System.EventHandler(this.查找替换编辑_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(187, 6);
            // 
            // 格式化编辑
            // 
            this.格式化编辑.Name = "格式化编辑";
            this.格式化编辑.Size = new System.Drawing.Size(190, 22);
            this.格式化编辑.Text = "格式化 (&D)";
            this.格式化编辑.Click += new System.EventHandler(this.解析按钮_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(187, 6);
            // 
            // 撤销编辑
            // 
            this.撤销编辑.Name = "撤销编辑";
            this.撤销编辑.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.撤销编辑.Size = new System.Drawing.Size(190, 22);
            this.撤销编辑.Text = "撤销 (&U)";
            this.撤销编辑.Click += new System.EventHandler(this.撤销_Click);
            // 
            // 重做编辑
            // 
            this.重做编辑.Name = "重做编辑";
            this.重做编辑.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.重做编辑.Size = new System.Drawing.Size(190, 22);
            this.重做编辑.Text = "重做 (&R)";
            this.重做编辑.Click += new System.EventHandler(this.重做_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(187, 6);
            // 
            // 复制编辑
            // 
            this.复制编辑.Name = "复制编辑";
            this.复制编辑.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.复制编辑.Size = new System.Drawing.Size(190, 22);
            this.复制编辑.Text = "复制 (&C)";
            this.复制编辑.Click += new System.EventHandler(this.复制_Click);
            // 
            // 粘贴编辑
            // 
            this.粘贴编辑.Name = "粘贴编辑";
            this.粘贴编辑.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.粘贴编辑.Size = new System.Drawing.Size(190, 22);
            this.粘贴编辑.Text = "粘贴 (&P)";
            this.粘贴编辑.Click += new System.EventHandler(this.粘贴_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(187, 6);
            // 
            // 全选编辑
            // 
            this.全选编辑.Name = "全选编辑";
            this.全选编辑.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.全选编辑.Size = new System.Drawing.Size(190, 22);
            this.全选编辑.Text = "全选 (&A)";
            this.全选编辑.Click += new System.EventHandler(this.全选_Click);
            // 
            // 帮助菜单
            // 
            this.帮助菜单.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.检查更新帮助,
            this.toolStripSeparator8,
            this.Gitee帮助,
            this.关于帮助});
            this.帮助菜单.Name = "帮助菜单";
            this.帮助菜单.Size = new System.Drawing.Size(65, 21);
            this.帮助菜单.Text = "帮助 (&H)";
            // 
            // 检查更新帮助
            // 
            this.检查更新帮助.Name = "检查更新帮助";
            this.检查更新帮助.Size = new System.Drawing.Size(180, 22);
            this.检查更新帮助.Text = "检查更新 (&U)";
            this.检查更新帮助.Visible = false;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(177, 6);
            this.toolStripSeparator8.Visible = false;
            // 
            // Gitee帮助
            // 
            this.Gitee帮助.Name = "Gitee帮助";
            this.Gitee帮助.Size = new System.Drawing.Size(180, 22);
            this.Gitee帮助.Text = "前往Gitee (&G)";
            this.Gitee帮助.Click += new System.EventHandler(this.Gitee帮助_Click);
            // 
            // 关于帮助
            // 
            this.关于帮助.Name = "关于帮助";
            this.关于帮助.Size = new System.Drawing.Size(180, 22);
            this.关于帮助.Text = "关于JsonList (&A)";
            this.关于帮助.Click += new System.EventHandler(this.关于帮助_Click);
            // 
            // 压缩按钮
            // 
            this.压缩按钮.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.压缩按钮.Location = new System.Drawing.Point(661, 69);
            this.压缩按钮.Name = "压缩按钮";
            this.压缩按钮.Size = new System.Drawing.Size(70, 38);
            this.压缩按钮.TabIndex = 4;
            this.压缩按钮.Text = "压缩";
            this.压缩按钮.UseVisualStyleBackColor = true;
            this.压缩按钮.Click += new System.EventHandler(this.压缩按钮_Click);
            // 
            // Json解析窗口
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(735, 810);
            this.Controls.Add(this.压缩按钮);
            this.Controls.Add(this.顶部菜单);
            this.Controls.Add(this.清除按钮);
            this.Controls.Add(this.解析按钮);
            this.Controls.Add(this.Json代码编辑框);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.顶部菜单;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Json解析窗口";
            this.Text = "JsonList";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Json解析窗口_FormClosing);
            this.Shown += new System.EventHandler(this.Json解析窗口_Shown);
            this.代码框右键菜单.ResumeLayout(false);
            this.顶部菜单.ResumeLayout(false);
            this.顶部菜单.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ICSharpCode.TextEditor.TextEditorControl Json代码编辑框;
        private System.Windows.Forms.Button 解析按钮;
        private System.Windows.Forms.Button 清除按钮;
        private System.Windows.Forms.ContextMenuStrip 代码框右键菜单;
        private System.Windows.Forms.MenuStrip 顶部菜单;
        private System.Windows.Forms.ToolStripMenuItem 文件菜单;
        private System.Windows.Forms.ToolStripMenuItem 打开文件;
        private System.Windows.Forms.ToolStripMenuItem 保存文件;
        private System.Windows.Forms.ToolStripMenuItem 另存为文件;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 退出文件;
        private System.Windows.Forms.ToolStripMenuItem 编辑菜单;
        private System.Windows.Forms.ToolStripMenuItem 查找替换编辑;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 复制编辑;
        private System.Windows.Forms.ToolStripMenuItem 粘贴编辑;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 全选编辑;
        private System.Windows.Forms.ToolStripMenuItem 查找替换右键;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 复制右键;
        private System.Windows.Forms.ToolStripMenuItem 粘贴右键;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 全选右键;
        private System.Windows.Forms.Button 压缩按钮;
        private System.Windows.Forms.ToolStripMenuItem 撤销编辑;
        private System.Windows.Forms.ToolStripMenuItem 重做编辑;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem 撤销右键;
        private System.Windows.Forms.ToolStripMenuItem 重做右键;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem 帮助菜单;
        private System.Windows.Forms.ToolStripMenuItem 检查更新帮助;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem Gitee帮助;
        private System.Windows.Forms.ToolStripMenuItem 关于帮助;
        private System.Windows.Forms.ToolStripMenuItem 格式化右键;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem 格式化编辑;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
    }
}

