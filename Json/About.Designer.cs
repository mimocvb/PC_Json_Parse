﻿
namespace Json
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.LogoIco = new System.Windows.Forms.PictureBox();
            this.Titles = new System.Windows.Forms.Label();
            this.Links = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LogoIco)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoIco
            // 
            this.LogoIco.Image = ((System.Drawing.Image)(resources.GetObject("LogoIco.Image")));
            this.LogoIco.Location = new System.Drawing.Point(20, 12);
            this.LogoIco.Name = "LogoIco";
            this.LogoIco.Size = new System.Drawing.Size(54, 68);
            this.LogoIco.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoIco.TabIndex = 0;
            this.LogoIco.TabStop = false;
            // 
            // Titles
            // 
            this.Titles.AutoSize = true;
            this.Titles.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Titles.Location = new System.Drawing.Point(113, 21);
            this.Titles.Name = "Titles";
            this.Titles.Size = new System.Drawing.Size(72, 16);
            this.Titles.TabIndex = 1;
            this.Titles.Text = "JsonList";
            // 
            // Links
            // 
            this.Links.AutoSize = true;
            this.Links.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Links.Font = new System.Drawing.Font("宋体", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Links.ForeColor = System.Drawing.Color.MediumBlue;
            this.Links.Location = new System.Drawing.Point(118, 57);
            this.Links.Name = "Links";
            this.Links.Size = new System.Drawing.Size(63, 14);
            this.Links.TabIndex = 2;
            this.Links.Text = "By：MIMO";
            this.Links.Click += new System.EventHandler(this.Links_Click);
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(222, 92);
            this.Controls.Add(this.Links);
            this.Controls.Add(this.Titles);
            this.Controls.Add(this.LogoIco);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "About";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "关于";
            ((System.ComponentModel.ISupportInitialize)(this.LogoIco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoIco;
        private System.Windows.Forms.Label Titles;
        private System.Windows.Forms.Label Links;
    }
}