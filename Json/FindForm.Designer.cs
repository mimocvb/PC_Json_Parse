﻿
namespace Json
{
    partial class FindForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindForm));
            this.查找内容标签 = new System.Windows.Forms.Label();
            this.替换标签 = new System.Windows.Forms.Label();
            this.查找内容编辑框 = new System.Windows.Forms.TextBox();
            this.替换编辑框 = new System.Windows.Forms.TextBox();
            this.查找按钮 = new System.Windows.Forms.Button();
            this.替换按钮 = new System.Windows.Forms.Button();
            this.全部替换按钮 = new System.Windows.Forms.Button();
            this.区分大小写选择框 = new System.Windows.Forms.CheckBox();
            this.循环选择框 = new System.Windows.Forms.CheckBox();
            this.方向分组框 = new System.Windows.Forms.GroupBox();
            this.向下单选框 = new System.Windows.Forms.RadioButton();
            this.向上单选框 = new System.Windows.Forms.RadioButton();
            this.方向分组框.SuspendLayout();
            this.SuspendLayout();
            // 
            // 查找内容标签
            // 
            this.查找内容标签.AutoSize = true;
            this.查找内容标签.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.查找内容标签.Location = new System.Drawing.Point(12, 18);
            this.查找内容标签.Name = "查找内容标签";
            this.查找内容标签.Size = new System.Drawing.Size(77, 14);
            this.查找内容标签.TabIndex = 0;
            this.查找内容标签.Text = "查找内容：";
            // 
            // 替换标签
            // 
            this.替换标签.AutoSize = true;
            this.替换标签.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.替换标签.Location = new System.Drawing.Point(12, 65);
            this.替换标签.Name = "替换标签";
            this.替换标签.Size = new System.Drawing.Size(63, 14);
            this.替换标签.TabIndex = 1;
            this.替换标签.Text = "替换为：";
            // 
            // 查找内容编辑框
            // 
            this.查找内容编辑框.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.查找内容编辑框.Location = new System.Drawing.Point(95, 12);
            this.查找内容编辑框.Name = "查找内容编辑框";
            this.查找内容编辑框.Size = new System.Drawing.Size(191, 30);
            this.查找内容编辑框.TabIndex = 2;
            this.查找内容编辑框.TextChanged += new System.EventHandler(this.查找内容编辑框_TextChanged);
            // 
            // 替换编辑框
            // 
            this.替换编辑框.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.替换编辑框.Location = new System.Drawing.Point(95, 56);
            this.替换编辑框.Name = "替换编辑框";
            this.替换编辑框.Size = new System.Drawing.Size(191, 30);
            this.替换编辑框.TabIndex = 3;
            // 
            // 查找按钮
            // 
            this.查找按钮.Location = new System.Drawing.Point(304, 12);
            this.查找按钮.Name = "查找按钮";
            this.查找按钮.Size = new System.Drawing.Size(95, 30);
            this.查找按钮.TabIndex = 4;
            this.查找按钮.Text = "查找下一个";
            this.查找按钮.UseVisualStyleBackColor = true;
            this.查找按钮.Click += new System.EventHandler(this.查找按钮_Click);
            // 
            // 替换按钮
            // 
            this.替换按钮.Location = new System.Drawing.Point(304, 55);
            this.替换按钮.Name = "替换按钮";
            this.替换按钮.Size = new System.Drawing.Size(95, 32);
            this.替换按钮.TabIndex = 5;
            this.替换按钮.Text = "替换";
            this.替换按钮.UseVisualStyleBackColor = true;
            this.替换按钮.Click += new System.EventHandler(this.替换按钮_Click);
            // 
            // 全部替换按钮
            // 
            this.全部替换按钮.Location = new System.Drawing.Point(304, 100);
            this.全部替换按钮.Name = "全部替换按钮";
            this.全部替换按钮.Size = new System.Drawing.Size(95, 32);
            this.全部替换按钮.TabIndex = 6;
            this.全部替换按钮.Text = "全部替换";
            this.全部替换按钮.UseVisualStyleBackColor = true;
            this.全部替换按钮.Click += new System.EventHandler(this.全部替换按钮_Click);
            // 
            // 区分大小写选择框
            // 
            this.区分大小写选择框.AutoSize = true;
            this.区分大小写选择框.Location = new System.Drawing.Point(11, 116);
            this.区分大小写选择框.Name = "区分大小写选择框";
            this.区分大小写选择框.Size = new System.Drawing.Size(84, 16);
            this.区分大小写选择框.TabIndex = 7;
            this.区分大小写选择框.Text = "区分大小写";
            this.区分大小写选择框.UseVisualStyleBackColor = true;
            // 
            // 循环选择框
            // 
            this.循环选择框.AutoSize = true;
            this.循环选择框.Checked = true;
            this.循环选择框.CheckState = System.Windows.Forms.CheckState.Checked;
            this.循环选择框.Location = new System.Drawing.Point(11, 142);
            this.循环选择框.Name = "循环选择框";
            this.循环选择框.Size = new System.Drawing.Size(48, 16);
            this.循环选择框.TabIndex = 8;
            this.循环选择框.Text = "循环";
            this.循环选择框.UseVisualStyleBackColor = true;
            // 
            // 方向分组框
            // 
            this.方向分组框.Controls.Add(this.向下单选框);
            this.方向分组框.Controls.Add(this.向上单选框);
            this.方向分组框.Location = new System.Drawing.Point(148, 101);
            this.方向分组框.Name = "方向分组框";
            this.方向分组框.Size = new System.Drawing.Size(138, 55);
            this.方向分组框.TabIndex = 9;
            this.方向分组框.TabStop = false;
            this.方向分组框.Text = "方向";
            // 
            // 向下单选框
            // 
            this.向下单选框.AutoSize = true;
            this.向下单选框.Checked = true;
            this.向下单选框.Location = new System.Drawing.Point(81, 25);
            this.向下单选框.Name = "向下单选框";
            this.向下单选框.Size = new System.Drawing.Size(47, 16);
            this.向下单选框.TabIndex = 1;
            this.向下单选框.TabStop = true;
            this.向下单选框.Text = "向下";
            this.向下单选框.UseVisualStyleBackColor = true;
            // 
            // 向上单选框
            // 
            this.向上单选框.AutoSize = true;
            this.向上单选框.Location = new System.Drawing.Point(16, 25);
            this.向上单选框.Name = "向上单选框";
            this.向上单选框.Size = new System.Drawing.Size(47, 16);
            this.向上单选框.TabIndex = 0;
            this.向上单选框.Text = "向上";
            this.向上单选框.UseVisualStyleBackColor = true;
            // 
            // FindForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 170);
            this.Controls.Add(this.方向分组框);
            this.Controls.Add(this.循环选择框);
            this.Controls.Add(this.区分大小写选择框);
            this.Controls.Add(this.全部替换按钮);
            this.Controls.Add(this.替换按钮);
            this.Controls.Add(this.查找按钮);
            this.Controls.Add(this.替换编辑框);
            this.Controls.Add(this.查找内容编辑框);
            this.Controls.Add(this.替换标签);
            this.Controls.Add(this.查找内容标签);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(425, 209);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(425, 209);
            this.Name = "FindForm";
            this.ShowInTaskbar = false;
            this.Text = "查找/替换";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FindForm_FormClosed);
            this.Load += new System.EventHandler(this.FindForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FindForm_KeyDown);
            this.方向分组框.ResumeLayout(false);
            this.方向分组框.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label 查找内容标签;
        private System.Windows.Forms.Label 替换标签;
        private System.Windows.Forms.TextBox 查找内容编辑框;
        private System.Windows.Forms.TextBox 替换编辑框;
        private System.Windows.Forms.Button 查找按钮;
        private System.Windows.Forms.Button 替换按钮;
        private System.Windows.Forms.Button 全部替换按钮;
        private System.Windows.Forms.CheckBox 区分大小写选择框;
        private System.Windows.Forms.CheckBox 循环选择框;
        private System.Windows.Forms.GroupBox 方向分组框;
        private System.Windows.Forms.RadioButton 向下单选框;
        private System.Windows.Forms.RadioButton 向上单选框;
    }
}