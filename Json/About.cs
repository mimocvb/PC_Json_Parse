﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Json
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void Links_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.gugugun.com/");
        }
    }
}
