﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Json
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if(args.Length > 0)
            {
                string path;

                if(args.Length == 1)
                {
                    path = args[0].ToString();
                }
                else
                {
                    foreach (string i in args)
                    {
                        System.Diagnostics.Process.Start(Application.ExecutablePath.ToString(), i);
                    }

                    return;
                }

                Application.Run(new Json解析窗口(path, Application.StartupPath));
            }
            else
            {
                注册扩展名关联();
                添加新建菜单项();
                Application.Run(new Json解析窗口(null, Application.StartupPath));
            }
        }

        static void 注册扩展名关联()
        {
            string keyName;
            string keyValue;
            keyName = "json";
            keyValue = "json";
            RegistryKey isExCommand = null;
            bool isCreateRegistry = true;

            try
            {
                /// 检查 文件关联是否创建 
                isExCommand = Registry.ClassesRoot.OpenSubKey(keyName);
                if (isExCommand == null)
                {
                    isCreateRegistry = true;
                }
                else
                {
                    if (isExCommand.GetValue("Create").ToString() == Application.ExecutablePath.ToString())
                    {
                        isCreateRegistry = false;
                    }
                    else
                    {
                        Registry.ClassesRoot.DeleteSubKeyTree(keyName);
                        isCreateRegistry = true;
                    }

                }
            }
            catch (Exception)
            {
                isCreateRegistry = true;
            }

            if (isCreateRegistry)
            {
                try
                {
                    RegistryKey key, keyico;
                    key = Registry.ClassesRoot.CreateSubKey(keyName);
                    key.SetValue("Create", Application.ExecutablePath.ToString());

                    keyico = key.CreateSubKey("DefaultIcon");
                    keyico.SetValue("", Application.ExecutablePath + ",0");

                    key.SetValue("", keyValue);
                    key = key.CreateSubKey("Shell");
                    key = key.CreateSubKey("Open");
                    key = key.CreateSubKey("Command");

                    /// 关联的位置 
                    key.SetValue("", Application.ExecutablePath.ToString() + @" %1/");
                }
                catch (Exception)
                {
                }
            }
        }

        static void 添加新建菜单项()
        {
            string keyName;
            string keyValue;
            keyName = ".json";
            keyValue = "NullFile";
            RegistryKey isExCommand = null;
            bool isCreateRegistry = true;

            try
            {
                /// 检查 文件关联是否创建 
                isExCommand = Registry.ClassesRoot.OpenSubKey(keyName);
                if (isExCommand == null)
                {
                    isCreateRegistry = true;
                }
                else
                {
                    if (isExCommand.GetValue("").ToString() == keyName)
                    {
                        isCreateRegistry = false;
                    }
                    else
                    {
                        Registry.ClassesRoot.DeleteSubKeyTree(keyName);
                        isCreateRegistry = true;
                    }

                }
            }
            catch (Exception)
            {
                isCreateRegistry = true;
            }

            if (isCreateRegistry)
            {
                try
                {
                    RegistryKey key, keyico;
                    key = Registry.ClassesRoot.CreateSubKey(keyName);
                    key.SetValue("", keyName);

                    keyico = key.CreateSubKey("ShellNew");
                    keyico.SetValue("NullFile", "");
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
