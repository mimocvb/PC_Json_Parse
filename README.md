# MIMO-桌面端Json解析格式化小程序

#### 介绍
Windows桌面端的Json解析格式化小程序

支持：代码折叠、Json高亮显示、解析错误键名提示、[ICSharpCode]的查找/替换、文件基础操作、自动关联[.Json]格式

.NET版本：4.0

第三方库： [Newtonsoft.Json] Json库

第三方组件：[ICSharpCode.TextEditor] 代码编辑框组件 

#### 效果图

![效果图](1.png)